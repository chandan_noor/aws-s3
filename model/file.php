<?php
require_once('include/class.upload.php');//integrating upload class of php
require_once('include/s3.php'); //integrating aws s3

class file
{
    private $category, $caption, $extension, $file, $directory;

    public function setCategory($category){
        $this->category = $category;
    }
    public function getCategory(){
        return $this->category;
    }
    public function setCaption($caption){
        $this->caption = $caption;
    }
    public function getCaption(){
        return $this->caption;
    }
    public function setDirectory($directory){
        $this->directory = $directory;
    }
    public function getDirectory(){
        return $this->directory;
    }
    public function setFile($file){
        $this->file = $file;
        $extension = explode('.', $file['name']);
        $extension = strtolower(end($extension));
        $this->setExtension($extension);
    }
    public function getFile(){
        return $this->file;
    }
    public function setExtension($extension){
        $this->extension = $extension;
    }
    public function getExtension(){
        return $this->extension;
    }
    public function uploadInTemp(){
        $file = $this->getFile();
        $handle = new upload($file);
        $handle->file_new_name_body = $this->getCaption();
        if ($handle->uploaded) {
            if ($handle->file_is_image) {
                $handle->image_watermark = '../model/logo.png';
                $handle->image_watermark_position = 'BR';
            }
            $handle->process($this->getDirectory());
            $handle->clean();
            return $handle->file_dst_name; //return stored file name in temp
        } else
            return false;
        }

    public function uploadInS3(){
        $s3 = new S3('access key', 'secret key');//connect s3 with key and secret key
        $uploadInTemp = $this->uploadInTemp();
        if ($uploadInTemp){
            $new_name = $uploadInTemp;
            $s3 = $s3->putObject(
                $s3->inputFile('../temp/' . $this->uploadInTemp(), false),
                's3.urosd.com' . '/' . $this->getCategory(),
                $new_name,
                S3::ACL_PUBLIC_READ,
                array(),
                array(),
                S3::STORAGE_CLASS_RRS
            );
            if($s3){
                unlink('../temp/' . $this->uploadInTemp());
                return 'Successfully Uploaded In S3';
            } else
                return 'not uploaded in S3';
        } else
            return 'not uploaded in temp';
    }
}