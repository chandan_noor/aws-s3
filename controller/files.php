<?php
require_once('../model/file.php');
$upload_file_obj = new file();
//$input, $bucket, $uri, $acl = self::ACL_PRIVATE, $metaHeaders = array(), $requestHeaders = array(), $storageClass = self::STORAGE_CLASS_STANDARD, $serverSideEncryption = self::SSE_NONE
$category = $_POST['category'];
$upload_file_obj->setCategory($category);
$caption = $_POST['caption'];
$upload_file_obj->setCaption($caption);
$file = $_FILES['file'];
$upload_file_obj->setFile($file);
$upload_file_obj->setDirectory($_SERVER['DOCUMENT_ROOT'].'aws_s3/temp/');
$confirmation = $upload_file_obj->uploadInS3();
header('location:../index.php?confirmation='.$confirmation);
